import React from "react";

import "./style.css";

const Footer = () => <footer>©{new Date().getFullYear()} steveostudios</footer>;

export default Footer;
