---
layout: post
title: Keeping Track of Ideas

---
When the moment an idea strikes I *have to* write it down. If I don't, it's gone! *Poof!* I've lost too many ideas because I think I'll remember it later and eventually just forget it.

Because of this, over the years I have tried a few methods of writing these things down as they come, but I haven't yet found *the system* that works for me.

What I am looking for is a list system that has several key things:

####Extremely fast
The first and foremost rule for the ideal system I am looking for is it has to be extremely fast. 

I am a slow typer, and I usually get ideas when I am running from one place to another, so I need to be able to type the idea in while waiting on an elevator or getting out of my car. 

####Cross-platform
I want a system that I can use on my phone (iPhone) and my computer (Mac). It's pretty rare that I don't have one of those on my person, so as long as I have that, I'm good. 

####Quick Syncing
I want it to sync over the internet, and to be super fast when it syncs.

####Integration with tools I laready use
Currently I use Dropbox, Evernote, and Sublime Text. Ideally I'd like this system to integrate with one of these, but I usually have Sublime Text open already on my computer, so it'd be awesome if it used that to work.

####*Not* feature-heavy
I don't want all of the bells and whistle. What I really want id the ability to make lists (checklists, thought lists, multiple lists, lists within lists), check off items, and set some due dates. That's it. 

####Cheap
I would love for it to be fairly cheap. I already use free services that sync (Evernote, Dropbox) so I'd like to use them as the service. I don't mind paying for an app, but the service I'd like free.

####Pretty
I'm vain. As a designer, I want this thing to not be offensive like a lot of list systems out there. I don't need dropshadows and skeuomporhistic graphics. But I do want a simple, clean, and functional design.

####Markdown support
This isn't necessary, but the more I learn hwo to use Markdown, the more I love it and it would be awesome to create lists using Markdown natively. 


- Nothing wrong with any of these, but I'm constantly looking for somehthing to meet my needs
		- Extremely fast
		- Crossplatform
		- syncing
		- relatively cheap
		- integrates with tools I already use
				- Sublime text
				- Evernote
		- *not* feature-heavy
		- Pretty
		- Would love to have Markdown support

		- Trello
		- Evernote
		- Plain text files
		- Reminders
		- Wunderlist
		- Listacular
		- And now Taskpaper