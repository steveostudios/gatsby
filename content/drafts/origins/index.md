---
title: Origins
date: 2019-06-06
---

Sidekick is an app that helps youth ministries create an interactive program.

My buddy Dave and I were making graphics for the Simply Youth Ministry conference. Josh Griffin, who was leading programming for the event, asked if we could make a special reusable element for the evening sessions

> _Josh_: Can you guys make a spinning wheel of prizes and it land on a prize?
>
> _Dave and I_: Sure! We can make a video of a wheel that lands on a prize. What prize do you want it to land on?
>
> _Josh_: Well, we want it to be _random_.
>
> _Dave and I_: Okay. We can make a video to land on each of the prizes and then randomly pick it in the booth. What do you want the prizes to be?
>
> _Josh_: I don't know yet. We won't know until the day of.
>
> _Dave and I_: Oh. Okay... well we can make a placeholder After Effects project and hit render that day... do you know _how many prizes_ you'll have?
>
> _Josh_: Nope.
>
> _Dave and I_: Okay... ummm... We'll figure something out...

We knew we couldn't do this with video... it had to be a "program". But we weren't programmers! Over the next few weeks I cobbled together one of the most insane things I have ever created. I had played around with Quartz Composer, a obscure (and now defunct) tool in Apple's Developer Tools that allowed you to program screen savers and visuals. Instead of using code, you used nodes and "noodles" to connect logic together. You could in real-time see what you were creating. Through hours of noodling around, I created a sufficient enough "app". You could have 2-15 prizes, edit their names, and then spin the wheel which would slow to a stop on a prize. To use it you had to install a bunch of stuff, and edit the noodles by hand!

The night it debuted I was standing by the "Games & Media" table in the store. Youth pastor after youth pastor asked where they could find "Wheel of Destiny" game. I had to tell them that there was no way they could run it in their service! On the flight home Dave and I dreamed about how we could make a wheel _anybody could use!_ We had no money, so we couldn't hire anyone to make it. Both of us had some Macromedia/Adobe Flash experience, so we decided to attempt to make an app with Adobe Flex (a way to use Flash to make Air apps).

I devoured tutorials, poured over documentation, took apart every free Adobe Flex app I could find and bought a 500 page _Adobe Flex 3_ book. Every night after work I tuck myself away and toil. The next day I would show Dave the mountain I scaithed the night before. "Look, you can type words here, and they show up \_over heeeere!".

10 months later, _Spin That Wheel_ was born. We debuted it at the Gurus of Tech conference, and then later at the next Simply Youth Ministry conference. It was a smash! We started dreaming -- what if we could do _other apps_, like Family Feud, countdowns, decibel meters! We could go CRAZY! And we did. We made _Ready, Set, Go!_ (a countdown maker), and _ScoreKeep_ (a digital scoreboard). We were almost done with _Survey Says_ (Family Feud) and _Gauge It_ (a live decibel meter) when Steve Jobs wrote his open letter about Apple not adopting Flash for iOS. While this didn't affect us initially, the writing on the walls was clear. If we wanted to keep making games, we needed a new technology.

I'll spare you the details. Be Digital Stache went dark. Adobe did so much for us. We experimented with working online only webpages, building apps for iOS only, and several platforms that promised a lot, and then sunset a few months later. It was hard times.
