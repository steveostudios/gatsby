---
title: Design Wars
date: "2019-03-23"
---

I'm old school. I started out designing websites in Photoshop (remember slicing up mockups) and dropping them into Flash or Dreamweaver! But design tools have changed!

A few years ago Sketch came out and it changed the game. Responsive websites were impossible to design in Photoshop. But with Sketch you could...

I went deep into Sketch. I designed some crazy intricate designs that had multi-state buttons, dynamic text, and responsive layouts. I even wrote some pretty popular Medium articles about my Sketch escapades.

Then I discovered Figma. Figma is super promising. The team has iterated faster. It is online, so giving developers designs doesn't mean exporting or requiring them to have a license. And it's free! The realtime collab os awesome! Organizing things is not possible.

Adobe is behind with XD, but they are moving in fast. I want to like XD -- I pay for it (since I still use a lot of other Adode CC products). I like the filesystem better than Figma's cloud based system (call me old school). Their asset viewer is the best one. But their components are basically unusable. It's not

<!-- [salted duck eggs](http://en.wikipedia.org/wiki/Salted_duck_egg). -->

<!-- ![Chinese Salty Egg](./salty_egg.jpg) -->
