---
layout: post
title: "Intro to console.log()"
tags:
- javascript
---
When writing Javascript it's hard to see what is *really* going behind the scenes. Console is

#### What is console
Chrome, open console

#### How to open it

#### console.log()
`console.log()` works to display anything you want in the Developer console. For instance:
```
console.log('Hello World!');
```
You can use it to check anything inside the console, not just strings:
```
var me = 'Steve';
console.log('Hello to'+ me);
```
sometimes when I am checking a variable, or multiple variables, I write it like this:
```
var me = 'Steve';
console.log('me', me);
```
You can also do perform Javascript inside console:
```
console.log(2+3);
```

#### console.table()
```
var
console.table
`
`

Console.count()

