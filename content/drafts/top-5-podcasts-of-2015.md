---
layout: post
title: "My Top 5 Podcasts of 2015"
---
I think that 2015 is the year of the podcast... again! I don't think it's just me; there has been a huge resurgence back into podcasting and with this second round it's so much better than the first. Here's my top 5  list of what I've been listening to the last 12 months.

#### Shop Talk Show
Think Click and Clack of famed _Car Talk_ on NPR, but with two nerds talking about web technologies. Said two nerds are Chris Coyier of CSS-tricks.com and CodePen, and Dave Rupert of Paravel. General episodes include live Q & A with web-developer-type celebrities. Super funny, very informative and downright cool.

#### History of Rome
While this podcast hasn't aired in several years, it's pretty incredible, and let's be honest, the content isn't changing a lot! I stumbled across this podcast on someone else's recommendation and am so glad I followed up on it. 
I remember bits and pieces of the history of Rome in middle school and high school, but couldn't have told you a lick about what happened. I'm not sure that after almost 200 episodes I can tell you a _whole_ lot more, but I
 
#### Slack Variety Pack 
Think audio-only variety show (hence the title) produced weekly by all of our beloved productivity app, Slack. With hilarious fake commercials, super-serious interviews, and productivity tips, 


#### 5 Minutes of JavaScript 

#### CodePen Radio
